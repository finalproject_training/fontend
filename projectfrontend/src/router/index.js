import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

export default new VueRouter({
  routes: [
    {
      path: '/home',
      name: 'Home',
      component: () => import('../views/user/Home.vue')
    },
    {
      path: '/login',
      name: 'Login',
      component: () => import('../views/Login.vue')
    },
    {
      path: '/history',
      name: 'History',
      component: () => import('../views/user/History.vue')
    },
    {
      path: '/admininfo',
      name: 'AdminInformation',
      component: () => import('../views/admin/Info.vue')
    },
    {
      path: '/admintrainreg/:id',
      name: 'AdminTrainReg',
      component: () => import('../views/admin/TrainManage.vue')
    },
    {
      path: '/programesmanage',
      name: 'ProgramesManage',
      component: () => import('../views/admin/ProgrameManage.vue')
    },
    {
      path: '/usersmanage',
      name: 'UsersManage',
      component: () => import('../views/admin/UsersManage.vue')
    }
  ]
})
