import { SET_USERS, RESET_USERS } from '../mutation-types'
import api from '../../services/api'

export default {
  namespaced: true,
  state: () => ({
    users: JSON.parse(localStorage.getItem('users'))
  }),
  mutations: {
    [SET_USERS] (state, payload) {
      state.users = payload
    },
    [RESET_USERS] (state) {
      state.users = null
    }
  },
  actions: {
    async getUsers ({ commit }) {
      try {
        const res = await api.get('/students/')
        const users = res.data
        localStorage.setItem('users', JSON.stringify(users))
        commit(SET_USERS, users)
        console.log(res)
      } catch (e) {
        console.log('Error')
      }
    },
    async addStudent ({ dispatch }, payload) {
      console.log('เพิ่มผู้ใช้')
      var email = payload.studentCode
      try {
        const res = await api.post('/students/add', {
          name: payload.name,
          programe: payload.programe,
          class: payload.class,
          studentCode: payload.studentCode
        })
        email += '@go.buu.ac.th'
        dispatch('addUser', { username: email, password: 'password' })
        dispatch('getUsers')
        console.log(res)
      } catch (e) {
        console.log('Error')
      }
    },
    async addUser ({ dispatch }, payload) {
      console.log('เพิ่มผู้ใช้')
      try {
        const res = await api.post('/users/add', {
          username: payload.username,
          password: payload.password
        })
        console.log(res)
      } catch (e) {
        console.log('Error')
      }
    }
  },
  getters: {
    getUsers (state, getters) {
      return state.users
    }
  }
}
