import { SET_PROGRAMES, RESET_PROGRAMES } from '../mutation-types'
import api from '../../services/api'

export default {
  namespaced: true,
  state: () => ({
    programes: JSON.parse(localStorage.getItem('programes'))
  }),
  mutations: {
    [SET_PROGRAMES] (state, payload) {
      state.programes = payload
    },
    [RESET_PROGRAMES] (state) {
      state.programes = null
    }
  },
  actions: {
    async getProgramesAll ({ commit }) {
      try {
        const res = await api.get('/programe')
        const programes = res.data
        localStorage.setItem('programes', JSON.stringify(programes))
        commit(SET_PROGRAMES, programes)
        console.log(res)
      } catch (e) {
        console.log('Error')
      }
    },
    async getProgramesTrue ({ commit }) {
      try {
        const res = await api.get('/programe/get')
        const programes = res.data
        localStorage.setItem('programes', JSON.stringify(programes))
        commit(SET_PROGRAMES, programes)
        console.log(res)
      } catch (e) {
        console.log('Error')
      }
    },
    async updateProgrames ({ dispatch }, payload) {
      console.log('อัปเดตสาขา')
      try {
        const res = await api.put('/programe/update', {
          _id: payload._id,
          name: payload.name,
          status: payload.status
        })
        dispatch('getProgramesAll')
        console.log(res)
      } catch (e) {
        console.log('Error')
      }
    },
    async addProgrames ({ dispatch }, payload) {
      console.log('เพิ่มสาขา')
      try {
        const res = await api.post('/programe/add', {
          name: payload.name
        })
        dispatch('getProgramesAll')
        console.log(res)
      } catch (e) {
        console.log('Error')
      }
    }
  },
  getters: {
    getProgrames (state, getters) {
      return state.programes
    }
  }
}
