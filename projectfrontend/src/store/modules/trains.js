import {
  SET_TRAINS,
  RESET_TRAINS,
  SET_TRAIN,
  RESET_TRAIN,
  SET_TRAIN_MEMBERS,
  RESET_TRAIN_MEMBERS,
  SET_TRAIN_QUEUES,
  RESET_TRAIN_QUEUES
} from '../mutation-types'
import api from '../../services/api'

export default {
  namespaced: true,
  state: () => ({
    trains: JSON.parse(localStorage.getItem('trains')),
    train: JSON.parse(localStorage.getItem('train')),
    train_members: JSON.parse(localStorage.getItem('train_members')),
    train_queues: JSON.parse(localStorage.getItem('train_queues'))
  }),
  mutations: {
    [SET_TRAINS] (state, payload) {
      state.trains = payload
    },
    [RESET_TRAINS] (state) {
      state.trains = null
    },
    [SET_TRAIN] (state, payload) {
      state.train = payload
    },
    [RESET_TRAIN] (state) {
      state.train = null
    },
    [SET_TRAIN_MEMBERS] (state, payload) {
      state.train_members = payload
    },
    [RESET_TRAIN_MEMBERS] (state) {
      state.train_members = null
    },
    [SET_TRAIN_QUEUES] (state, payload) {
      state.train_queues = payload
    },
    [RESET_TRAIN_QUEUES] (state) {
      state.train_queues = null
    }
  },
  actions: {
    async getTrains ({ commit }) {
      try {
        const res = await api.get('/trains/')
        const trains = res.data
        localStorage.setItem('trains', JSON.stringify(trains))
        commit(SET_TRAINS, trains)
        console.log(res)
      } catch (e) {
        console.log('Error')
      }
    },
    async getTrainTitle ({ commit }, payload) {
      console.log(payload)
      try {
        const res = await api.post('/trains/', { title: payload.title })
        const train = res.data
        localStorage.setItem('train', JSON.stringify(train))
        commit(SET_TRAIN, train)
        console.log(res)
      } catch (e) {
        console.log('Error')
      }
    },
    async getTrainID ({ commit, dispatch }, payload) {
      const id = payload._id
      console.log(payload)
      try {
        const res = await api.get('/trains/' + id)
        const train = res.data
        localStorage.setItem('train', JSON.stringify(train))
        commit(SET_TRAIN, train)
        setTimeout(() => {
          dispatch('analyzeMember')
        }, 100)
        console.log(res)
      } catch (e) {
        console.log('Error')
      }
    },
    async addTrain ({ commit, dispatch }, payload) {
      console.log(payload)
      try {
        const res = await api.post('/trains/add', {
          _id: payload._id,
          title: payload.title,
          description: payload.description,
          conditions: payload.conditions,
          narrator: payload.narrator,
          channel: payload.channel,
          dateTrainStart: payload.dateTrainStart,
          dateTrainEnd: payload.dateTrainEnd,
          type: payload.type,
          amount: payload.amount,
          members: payload.members,
          countApprove: payload.countApprove,
          datePost: payload.datePost
        })
        dispatch('getTrains')
        console.log(res)
        commit(SET_TRAIN, payload)
      } catch (e) {
        console.log('Error')
      }
    },
    async updateTrain ({ commit, dispatch }, payload) {
      console.log('อัปเดตอบรม')
      try {
        const res = await api.put('/trains/update', {
          _id: payload._id,
          title: payload.title,
          description: payload.description,
          conditions: payload.conditions,
          narrator: payload.narrator,
          type: payload.type,
          channel: payload.channel,
          dateTrainStart: payload.dateTrainStart,
          dateTrainEnd: payload.dateTrainEnd,
          amount: payload.amount,
          members: payload.members,
          countApprove: payload.countApprove,
          datePost: payload.datePost,
          isShow: payload.isShow
        })
        console.log(res)
      } catch (e) {
        console.log('Error')
      }
    },
    async closePost ({ dispatch }, payload) {
      const train = payload
      train.isShow = false
      console.log(train)
      dispatch('updateTrain', {
        _id: train._id,
        title: train.title,
        description: train.description,
        conditions: train.conditions,
        narrator: train.narrator,
        type: payload.type,
        channel: train.channel,
        dateTrainStart: train.dateTrainStart,
        dateTrainEnd: train.dateTrainEnd,
        amount: train.amount,
        members: train.members,
        countApprove: payload.countApprove,
        datePost: train.datePost,
        isShow: train.isShow
      })
    },
    async setTrain ({ commit }, payload) {
      console.log(payload)
      const train = {
        _id: payload._id,
        title: payload.title,
        description: payload.description,
        conditions: payload.conditions,
        narrator: payload.narrator,
        type: payload.type,
        channel: payload.channel,
        dateTrainStart: payload.dateTrainStart,
        dateTrainEnd: payload.dateTrainEnd,
        amount: payload.amount,
        members: payload.members,
        countApprove: payload.countApprove
      }
      localStorage.setItem('train', JSON.stringify(train))
      commit(SET_TRAIN, train)
    },
    async clearTrain ({ commit }) {
      console.log('clear')
      localStorage.removeItem('train')
      localStorage.removeItem('train_members')
      localStorage.removeItem('train_queues')
      commit(RESET_TRAIN)
      commit(RESET_TRAIN_MEMBERS)
      commit(RESET_TRAIN_QUEUES)
    },
    analyzeMember ({ commit }) {
      console.log('a')
      const train = JSON.parse(localStorage.getItem('train'))
      var trainMembers = []
      var trainQueues = []
      for (let i = 0; i < train.members.length; i++) {
        if (train.members[i].status === 'อนุมัติ') {
          trainMembers.push(train.members[i])
        } else if (train.members[i].status === 'รออนุมัติ') {
          trainQueues.push(train.members[i])
        }
      }
      commit(SET_TRAIN_MEMBERS, trainMembers)
      commit(SET_TRAIN_QUEUES, trainQueues)
      localStorage.setItem('train_members', JSON.stringify(trainMembers))
      localStorage.setItem('train_queues', JSON.stringify(trainQueues))
    }
  },
  getters: {
    trains (state, getters) {
      return state.trains
    },
    train (state, getters) {
      return state.train
    },
    trainMembers (state, getters) {
      return state.train_members
    },
    trainQueues (state, getters) {
      return state.train_queues
    }
  }
}
