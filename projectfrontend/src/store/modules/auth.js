import {
  AUTH_LOGIN,
  AUTH_LOGOUT,
  SET_ADMIN,
  RESET_ADMIN
} from '../mutation-types'
import api from '../../services/api'
import router from '../../router'
import store from '../../store'

export default {
  namespaced: true,
  state: () => ({
    user: JSON.parse(localStorage.getItem('user')),
    admin: localStorage.getItem('admin')
  }),
  mutations: {
    [AUTH_LOGIN] (state, payload) {
      state.user = payload
    },
    [AUTH_LOGOUT] (state) {
      state.user = null
    },
    [SET_ADMIN] (state) {
      state.admin = true
    },
    [RESET_ADMIN] (state) {
      state.admin = false
    }
  },
  actions: {
    async login ({ commit, dispatch }, payload) {
      console.log(payload)
      try {
        const res = await api.post('/auth/login', {
          username: payload.email,
          password: payload.password
        })
        const user = res.data.user
        const token = res.data.token
        localStorage.setItem('token', token)
        localStorage.setItem('user', JSON.stringify(user))
        console.log(res)
        store.dispatch('trains/getTrains')
        setTimeout(() => {
          dispatch('checkAdmin')
        }, 400)
        commit(AUTH_LOGIN, user)
      } catch (e) {
        console.log('Error')
      }
    },
    logout ({ commit }) {
      localStorage.removeItem('token')
      localStorage.removeItem('user')
      localStorage.removeItem('studentCode')
      localStorage.removeItem('student')
      localStorage.removeItem('train')
      localStorage.removeItem('trains')
      localStorage.removeItem('admin')
      localStorage.removeItem('programes')
      localStorage.removeItem('users')
      commit(AUTH_LOGOUT)
      commit(RESET_ADMIN)
      router.push('login')
    },
    async checkAdmin ({ commit, getters }) {
      const user = JSON.parse(localStorage.getItem('user'))
      const roles = user.roles
      for (let i = 0; i < roles.length; i++) {
        if (roles[i] === 'ADMIN') {
          console.log(roles[i])
          localStorage.setItem('admin', true)
          commit(SET_ADMIN)
          router.push('admininfo')
        }
      }
      console.log(getters.isAdmin)
      if (!getters.isAdmin) {
        const studentCode = user.username.split('@go.buu.ac.th', 1)
        localStorage.setItem('studentCode', studentCode)
        store.dispatch('student/getStudent')
        setTimeout(() => {
          router.push('home')
        }, 350)
      }
      store.dispatch('trains/getTrains')
    }
  },
  getters: {
    isLogin (state, getters) {
      return state.user != null
    },
    isAdmin (state, getters) {
      return state.admin
    }
  }
}
