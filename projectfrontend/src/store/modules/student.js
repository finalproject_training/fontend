import { SET_STUDENT, RESET_STUDENT } from '../mutation-types'
import api from '../../services/api'

export default {
  namespaced: true,
  state: () => ({
    student: JSON.parse(localStorage.getItem('student'))
  }),
  mutations: {
    [SET_STUDENT] (state, payload) {
      state.student = payload
    },
    [RESET_STUDENT] (state) {
      state.student = null
    }
  },
  actions: {
    async getStudent ({ commit }) {
      const studentCode = JSON.parse(localStorage.getItem('studentCode'))
      console.log(studentCode)
      try {
        const res = await api.post('/students/', {
          studentCode: studentCode
        })
        const student = res.data
        localStorage.setItem('student', JSON.stringify(student))
        commit(SET_STUDENT, student)
        console.log(res)
      } catch (e) {
        console.log('Error')
      }
    },
    async updateStudent ({ commit, dispatch }, payload) {
      console.log('อัปเดตนักเรียน')
      try {
        const res = await api.put('/students/update', {
          _id: payload._id,
          studentCode: payload.studentCode,
          name: payload.name,
          programe: payload.programe,
          class: payload.class,
          trainHistories: payload.trainHistories
        })
        localStorage.setItem('student', JSON.stringify(payload))
        dispatch('getStudent')
        console.log(res)
      } catch (e) {
        console.log('Error')
      }
    }
  },
  getters: {
    studentCode (state, getters) {
      return state.student.studentCode
    },
    name (state, getters) {
      return state.student.name
    },
    trainHistories (state, getters) {
      return state.student.trainHistories
    }
  }
}
