/* eslint-disable no-const-assign */
import { SET_REGIS, RESET_REGIS } from '../mutation-types'
import store from '..'

export default {
  namespaced: true,
  state: () => ({
    regis: false
  }),
  mutations: {
    [SET_REGIS] (state) {
      state.regis = true
    },
    [RESET_REGIS] (state) {
      state.regis = false
    }
  },
  actions: {
    async checkRegis ({ commit }) {
      const student = JSON.parse(localStorage.getItem('student'))
      const train = JSON.parse(localStorage.getItem('train'))
      for (let i = 0; i < student.trainHistories.length; i++) {
        if (student.trainHistories[i].trainPost === train._id) {
          console.log('true')
          commit(SET_REGIS)
        }
      }
    },
    async clearRegis ({ commit }) {
      commit(RESET_REGIS)
    },
    async register ({ commit, getters }) {
      console.log('ลงทะเบียน')
      const student = JSON.parse(localStorage.getItem('student'))
      const train = JSON.parse(localStorage.getItem('train'))
      const conditionPrograme = train.conditions.programe
      const conditionClass = train.conditions.class
      for (let i = 0; i < conditionPrograme.length; i++) {
        if (student.programe.indexOf(conditionPrograme[i]) >= 0) {
          for (let j = 0; j < conditionClass.length; j++) {
            if (student.class.indexOf(conditionClass[j]) >= 0) {
              console.log('อนุมัติ')
              setTimeout(() => {
                const trainHistory = {
                  trainPost: train._id,
                  title: train.title,
                  status: 'อนุมัติ'
                }
                const member = {
                  studentCode: student.studentCode,
                  name: student.name,
                  programe: student.programe,
                  class: student.class,
                  status: 'อนุมัติ'
                }
                console.log(train)
                var countApprove = train.countApprove
                console.log(countApprove)
                countApprove += 1
                console.log(countApprove)
                student.trainHistories.push(trainHistory)
                train.members.push(member)
                train.countApprove = countApprove
                console.log(train.countApprove)
                store.dispatch('student/updateStudent', {
                  _id: student._id,
                  studentCode: student.studentCode,
                  name: student.name,
                  programe: student.programe,
                  class: student.class,
                  trainHistories: student.trainHistories
                })
                store.dispatch('trains/updateTrain', {
                  _id: train._id,
                  title: train.title,
                  description: train.description,
                  conditions: train.conditions,
                  narrator: train.narrator,
                  channel: train.channel,
                  dateTrainStart: train.dateTrainStart,
                  dateTrainEnd: train.dateTrainEnd,
                  amount: train.amount,
                  countApprove: train.countApprove,
                  members: train.members,
                  datePost: train.datePost
                })
              }, 400)
              commit(SET_REGIS)
              break
            }
          }
        }
      }
      if (!getters.isRegis) {
        console.log('รออนุมัติ')
        setTimeout(() => {
          const trainHistory = {
            trainPost: train._id,
            title: train.title,
            status: 'รออนุมัติ'
          }
          const member = {
            studentCode: student.studentCode,
            name: student.name,
            programe: student.programe,
            class: student.class,
            status: 'รออนุมัติ'
          }
          student.trainHistories.push(trainHistory)
          train.members.push(member)
          store.dispatch('student/updateStudent', {
            _id: student._id,
            studentCode: student.studentCode,
            name: student.name,
            programe: student.programe,
            class: student.class,
            trainHistories: student.trainHistories
          })
          store.dispatch('trains/updateTrain', {
            _id: train._id,
            title: train.title,
            description: train.description,
            conditions: train.conditions,
            narrator: train.narrator,
            channel: train.channel,
            dateTrainStart: train.dateTrainStart,
            dateTrainEnd: train.dateTrainEnd,
            amount: train.amount,
            countApprove: train.countApprove,
            members: train.members,
            datePost: train.datePost
          })
        }, 400)
      }
    },
    async approve ({ dispatch }, payload) {
      var train = JSON.parse(localStorage.getItem('train'))
      var countApprove = train.countApprove
      const studentCode = payload.studentCode
      localStorage.setItem('studentCode', JSON.stringify(studentCode))
      store.dispatch('student/getStudent')
      console.log(train)
      setTimeout(() => {
        var student = JSON.parse(localStorage.getItem('student'))
        const index = student.trainHistories.findIndex(
          e => e.trainPost === train._id
        )
        student.trainHistories[index].status = 'อนุมัติ'

        const indexTrain = train.members.findIndex(
          e => e.studentCode === studentCode
        )
        train.members[indexTrain].status = 'อนุมัติ'
        countApprove += 1
        train.countApprove = countApprove
        console.log(train)
        console.log(student)
        store.dispatch('student/updateStudent', {
          _id: student._id,
          studentCode: student.studentCode,
          name: student.name,
          programe: student.programe,
          class: student.class,
          trainHistories: student.trainHistories
        })
        store.dispatch('trains/updateTrain', {
          _id: train._id,
          title: train.title,
          description: train.description,
          conditions: train.conditions,
          narrator: train.narrator,
          channel: train.channel,
          dateTrainStart: train.dateTrainStart,
          dateTrainEnd: train.dateTrainEnd,
          amount: train.amount,
          countApprove: train.countApprove,
          members: train.members,
          datePost: train.datePost
        })
      }, 100)
    },
    async reject ({ dispatch }, payload) {
      const train = JSON.parse(localStorage.getItem('train'))
      const studentCode = payload.studentCode
      localStorage.setItem('studentCode', JSON.stringify(studentCode))
      store.dispatch('student/getStudent')

      setTimeout(() => {
        const student = JSON.parse(localStorage.getItem('student'))
        const index = student.trainHistories.findIndex(
          e => e.trainPost === train._id
        )
        student.trainHistories[index].status = 'ไม่อนุมัติ'

        const indexTrain = train.members.findIndex(
          e => e.studentCode === studentCode
        )
        train.members[indexTrain].status = 'ไม่อนุมัติ'
        console.log(train)
        console.log(student)
        store.dispatch('student/updateStudent', {
          _id: student._id,
          studentCode: student.studentCode,
          name: student.name,
          programe: student.programe,
          class: student.class,
          trainHistories: student.trainHistories
        })
        store.dispatch('trains/updateTrain', {
          _id: train._id,
          title: train.title,
          description: train.description,
          conditions: train.conditions,
          narrator: train.narrator,
          channel: train.channel,
          dateTrainStart: train.dateTrainStart,
          dateTrainEnd: train.dateTrainEnd,
          amount: train.amount,
          members: train.members,
          datePost: train.datePost
        })
      }, 300)
    },
    async addMember ({ getters }, payload) {
      console.log(payload)
      const studentCode = payload.studentCode
      localStorage.setItem('studentCode', studentCode)
      setTimeout(() => {
        store.dispatch('student/getStudent')
      }, 200)

      if (!getters.isRegis) {
        setTimeout(() => {
          const student = JSON.parse(localStorage.getItem('student'))
          const train = JSON.parse(localStorage.getItem('train'))
          var countApprove = train.countApprove
          const trainHistory = {
            trainPost: train._id,
            title: train.title,
            dateTrain: train.dateTrain,
            status: 'อนุมัติ'
          }
          const member = {
            studentCode: student.studentCode,
            name: student.name,
            programe: student.programe,
            class: student.class,
            status: 'อนุมัติ'
          }

          student.trainHistories.push(trainHistory)
          train.members.push(member)
          countApprove += 1
          train.countApprove = countApprove
          store.dispatch('student/updateStudent', {
            _id: student._id,
            studentCode: student.studentCode,
            name: student.name,
            programe: student.programe,
            class: student.class,
            trainHistories: student.trainHistories
          })

          store.dispatch('trains/updateTrain', {
            _id: train._id,
            title: train.title,
            description: train.description,
            conditions: train.conditions,
            narrator: train.narrator,
            channel: train.channel,
            dateTrainStart: train.dateTrainStart,
            dateTrainEnd: train.dateTrainEnd,
            amount: train.amount,
            members: train.members,
            countApprove: train.countApprove,
            datePost: train.datePost
          })
        }, 500)
      } else {
        console.log('เคยลงทะเบียนแล้ว')
      }
    }
  },
  getters: {
    isRegis (state, getters) {
      return state.regis
    }
  }
}
