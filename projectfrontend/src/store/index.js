import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
import student from './modules/student'
import trains from './modules/trains'
import programes from './modules/programes'
import users from './modules/users'
import service from './service/service'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    auth,
    student,
    trains,
    programes,
    users,
    service
  }
})
